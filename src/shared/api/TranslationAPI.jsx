import { Headers } from "./ApiHeader";

const apiUrl = process.env.REACT_APP_API_URL

console.log(apiUrl);

export const checkUser = async(username) => {
    try{
        const response = await fetch(`${apiUrl}?username=${username}`)
        console.log(response)
        if(!response.ok){
            throw new Error('request failed')
        }
        const data = await response.json()
        return data;
    }
    catch (error){
       return [error.message, []]
    }
}

export const createUser = async (username) => {
    try{
        if (username.length < 3) {
            alert("Username is to short, minimum length is 3.")
            return
        }
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: Headers(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Username' + username + 'already taken or invalid')
        }
        const data = await response.json()
        console.log(data)
        return sessionStorage.setItem("user", JSON.stringify(data))
    }
    catch(error){
        return [error.message, []]
    }
}

export const loginUser = async username => {
    const user = await checkUser(username)
    if (user.length !== 0) return sessionStorage.setItem(user[0].username, JSON.stringify(user))
    return await createUser(username)
}

export const addTranslation = async (user, translation) => {
    console.log(user);
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PUT',
            headers: Headers(),
            body: JSON.stringify({
                username: user.username,
                translations: translation
            })
        })
        if(!response.ok) throw new Error("Could not update translations")
        const data = await response.json()
        console.log(data);
        // const response = await fetch(`${apiUrl}/${user.id}`, {
        //     method: 'PUT',
        //     headers: Headers(),
        //     body: JSON.stringify({
        //         username: user.username,
        //         translations: [...user.translations, translation]
        //     })
        // })
        // console.log(response);
        // if(!response.ok){
        //     throw new Error('Could not update translations')
        // }
        // const result = await response.json()
        // return [null, result]
    } catch (error) {
        return [error.message, null]
    }
}