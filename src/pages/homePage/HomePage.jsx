import React from "react";
import Header from "../../components/header/Header";
import LoginBox from "../../components/loginBox/LoginBox";


import "./HomePage.css"

const HomePage = () => {

    return (
      <div>
        <Header />
        <main className="indexMain">
            <LoginBox/>
        </main>
      </div>
    );
}
export default HomePage