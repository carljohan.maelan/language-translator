import Header from '../../components/header/Header'
import Translation from '../../components/translation/Translation'
import UserButton from '../../components/userButton/userButton'

const TranslationPage = () => {

    return(
        <div>
        <Header>
            <UserButton/>
        </Header>
        <Translation />
        </div>
    )
}

export default TranslationPage