import { BrowserRouter, Route, Routes } from "react-router-dom"
import RoutingPaths from "./RoutingPaths"
import HomePage from "../pages/homePage/HomePage"
import TranslationPage from "../pages/translationPage/TranslationPage"
import UserPage from "../pages/userPage/userPage"

const PageRoutes = ({children}) => {
    return (
        <BrowserRouter>
            {children}
            <Routes>
                <Route exact path={RoutingPaths.translationPage} element={<TranslationPage />} />
                <Route path={RoutingPaths.home} element={<HomePage />} />
                <Route path={RoutingPaths.userPage} element={<UserPage />} />
                <Route path="*" element={<HomePage />} />
            </Routes>
        </BrowserRouter>
    )
}

export default PageRoutes