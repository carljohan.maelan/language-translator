const home = "/"
const translationPage = "/translation"
const userPage = "/user"

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    home,
    translationPage,
    userPage
}