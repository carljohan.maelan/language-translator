import './HistoryCard.css'

// import {useEffect, useState} from "react";
import { useUser } from '../../shared/context/UserContext'


const HistoryCard = () => {
    const {user} = useUser()
    let keys = Object.keys(sessionStorage);
    console.log(keys)

    const getHistory = sessionStorage.getItem(user)
    const res = JSON.parse(getHistory);

    function selectWhere(getHistory, propertyName) {
        for (let i = 0; i < getHistory.length; i++) {
            if(getHistory[i][propertyName] !== null) return getHistory[i][propertyName];
        }
        return null;
    }
    var getValue = selectWhere(res, "translations")
    const finalValue = [];
    console.log(getValue)
    for (let i = 0; i < getValue.length; i++) {
        finalValue.push(<div className="history-item">{getValue[i]}</div>);
    }
    return <div>{finalValue}</div>
}
export default HistoryCard