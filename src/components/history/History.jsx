import './History.css';
import {Link, useNavigate} from 'react-router-dom';
import HistoryCard from './HistoryCard';
import { useUser } from '../../shared/context/UserContext';
import { useEffect } from 'react';

const BackIcon = "<";


const HistoryList = ({translations}) => {
  const { user, setUser } = useUser();
  const navigate = useNavigate()
  
  // const [user, setUser] = useUser();
  // console.log(user)

    useEffect(() => {
      if (sessionStorage.getItem(user) === null) { navigate("/")}
      else history()
    }, [])

    const history = () => {
        return <HistoryCard />
    }

    return (
      <>
      <Link to='/translation'>
        <button  className="backButton">{BackIcon}</button>
        </Link>
        <div className="history">
          <div className="history-container">
            <h1 className="title"> History</h1>
            <div className="history-cards">
              {history()}
            </div>
          </div>
        </div>
      </>
    );
}

export default HistoryList