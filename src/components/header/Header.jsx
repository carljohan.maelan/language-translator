import './Header.css'
import Waves from '../../assets/images/Waves.png'

const Header = ({children}) => {

    return (

      <>
          <header className="header-container">
            <h1 className="header-title">Lost in Translation</h1>
            {children}
          </header>
            <img className="waves" src={Waves} alt="SVG as an img" />
      </>

    );
}

export default Header