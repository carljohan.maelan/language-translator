import { useNavigate } from "react-router-dom";
import { loginUser, checkUser } from "../../shared/api/TranslationAPI";
import { useState } from "react";
import LogoHello from "../../assets/images/Logo-Hello.png";
import "./LoginBox.css";
import { useUser } from "../../shared/context/UserContext";

const LoginBox = () => {
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    const [username, setUsername] = useState(" ");

    const handleInput = (event) => {
        setUsername(event.target.value);
    };
    async function onLogin() {
        checkLoggedIn();
        await setUser(username);
        await loginUser(username);
    }

    const checkLoggedIn = () => {
        if (sessionStorage.getItem(username) === null) {
            navigate("/");
        } else {
            navigate("/translation");
        }
    };

    return (
        <div className="login">
            <img className="logoHello" src={LogoHello} alt="Hello Logo" />
            <div className="box-column">
                <div>
                    <div className="box-button">
                        <h3>Username</h3>
                        <input
                            className="usernameInput"
                            type="text"
                            placeholder="Lil Robot"
                            onChange={handleInput}
                        />
                        <button onClick={() => onLogin()} className="loginBtn">
                            Login
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginBox;
