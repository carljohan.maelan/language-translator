# Language Translator

Language Translator is an application that converts English words/phrases to American sign language.

## Description
This is a simple translator which you can create a user by entering a username on the index page, and login using that same username if you've already created a user. Once you're logged in you can enter letters/words in the translation page which in turn will return translated pictures in American sign language. On the user-button in the top-right corner you can view your 10 previous translations.

Happy translating!

## Installation

```
npm-install
```

```
npm-start
```


## Usage


Hidden keys:
In order to access the API you need a API-key aswell as a API-Url. Create a .env file in the root of the solution and add .env to .gitignore. The API was cloned from dewalds repository([dewald-els](https://github.com/dewald-els/noroff-assignment-api))

```
..._API_KEY = <your key>
```
```
..._API_URL = <your url>
```

[HomePage](https://ibb.co/84qMMVF)
At the Homepage you will be faced with a input field in which you need to either login as an existing user with your username, or create a new one with a username of your choosing. 

[TranslationPage:](https://ibb.co/m65GDLp)
At the Translationpage you can type in English letters/words/phrases which in turn will translate it to American Sign language using emoji-handgestures for you. 

[UserPage:](https://ibb.co/23F4WQn)
At the Userpage you can view your 'History' which is your latest searched words from the TranslationPage. You will also be able to clear your History if you wish to do so.

Logout:
Once you're done using the application you can logout which will return you to the Homepage.

## Authors
Jeremy Matthiessen([Jerry585](https://gitlab.com/Jerry585))
Carl-Johan Maelan([carljohan.maelan](https://gitlab.com/carljohan.maelan))


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

